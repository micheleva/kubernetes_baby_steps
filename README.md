# Kubernetes baby steps - Quickly Navigate Kubernetes with Ease 🌱

![GPL v.3 license](https://img.shields.io/badge/license-GPL%20v3.0-brightgreen.svg "GPL v.3 License")

Requisites 🛠️
===

The following commands should be installed:

- `kubectl`
- `docker compose` or `docker-compose`
- `helm`

Other considerations:

- You must have root access in order to mount NFS exports
- `/tmp/nfs-root` is the hardcoded path to store the NFS exports
- Tested **only on Fedora 36**


Kind Cluster Setup ⚙️
===

# Quickstart 🚀
```
cd scripts/ && ./00_setup.sh && ./01_install_nfs-subdir-external_provisioner_retain_policy.sh && ./02_add_metrics.sh && ./03_add_dashboard.sh 
```

# Step by step 🔍
```
cd scripts

# Confirm all the needed files are installed, spin up a pod exporting an nfs, and start kind
./00_setup.sh

# Setup the nfs subdir external provisioner
./01_install_nfs-subdir-external_provisioner_retain_policy.sh

# Add metrics server to be able to use hpa
./02_add_metrics.sh

# Add the dashboard to visualize the usage data, or simply to perform admin task from GUI
./03_add_dashboard.sh
```

nfs subdir external provisioner demo 📁
===

A demo application is deployed in demo namespace.

This application is using a RWX persistent volume claim:
```
kubectl get all -n demo
kubectl get pv,pvc -n demo

```

Execute the following script to have both pods to write to the same volume.
The same script will also show the content of said file.
This demonstrte the nfs subdir external provider is able to provision RXW volumes.
```
./demo/01_demonstrate_volume_is_shared.sh
```

Confirm that the nfs volume really has said data.
```
cat /tmp/nfs-root/demo-test-claim-pvc-<random-id>/hoge.txt
```

hpa demo 📈
===

Deploy an nginx deployment with an Horizontal Pod Autoscaling in the nginx demo.
```
kubectl apply -f resources/nginx/nginx-hpa.yaml -n nginx
```

The deployment should initially have no pods, but almost immediately the HPA will scale up the deployment to 2 pods.
```
kubectl get all -n nginx
```
Give some time for the HPA to gather metrics about the pods.

Execute the folowing command in a different terminal, and let this second terminal running.
```
watch -n1 kubectl get all -n nginx 
```

Verify the HPA really scales up the number of pods when the CPU usage of the target pod is spiking.
Let's run sha256sum against /dev/random from the original terminal
This should generate quite a lot of load (you might be hearing your workmachine fan spinning hard!)
```
kubectl get pod -n nginx -o name
kubectl exec -n nginx <pod-name> -- sha256sum /dev/random 
```

Repeat the above step against the other pod (in other terminals, or simply add an & at the end of the line).

e.g.
```
kubectl get pods -n nginx --no-headers -o custom-columns=":metadata.name" |   xargs -I{} kubectl exec -n nginx {} -- sha256sum /dev/random &
```

After a few moments, in the second terminal, it can be observed hat the HPA noticed a full load on the 2 original pods, and thus has spinned up a few extra pods as requested.

(Optional) keep execuring the same command against the other new spawned pods.


hpa demo cleanup 🧹
===
Hit Ctrl-C in the original terminal (and in other terminals, if you did execute said steps multiple times).

If the xargs command, as shown above, has been executed clean up the foregroung processes as follows

```
pgrep kubectl | sudo xargs kill -9
pgrep sha56sum | sudo xargs kill -9
```

Confirm there are no more foreground jobs left
```
jobs
ps aux | grep -E 'kubectl|sha256sum'
```

dns demo 🌐
===

TODO

dns demo cleanup 🧹
===

TODO

Kind Cluster Removal 🗑️
===


```
cd scripts

# Delete the application and its PVC
./cleanup/01_delete_demo_app_and_check_pv_state.sh

# Remove the nfs subdir external provisioner
./cleanup/02_remove_nfs-subdir-external_provisioner.sh

# Delete demo pvc
./cleanup/03_delete_demo_pv.sh

# Stop the nfs exporting pod
./cleanup/04_stop_and_remove_nfs_exporting_pod.sh

# Note: the files in /tmp/nfs-export will not be deleted automatically. Please clean up yourself if you do not need them anymore!

# Remove the kubernetes cluster
./07_delete_kind_cluster.sh

# Clean up the retained pvs data
sudo rm -ri /tmp/nfs-root
```


📚 References
===

- https://kubernetes.io/docs/concepts/storage/persistent-volumes/
- https://kind.sigs.k8s.io/docs/user/quick-start/
- https://github.com/kubernetes-sigs/kind/issues/1487#issuecomment-616891081
- https://github.com/kubernetes-sigs/nfs-subdir-external-provisioner
- https://github.com/ehough/docker-nfs-server
- https://kind.sigs.k8s.io/docs/user/resources/#testing-your-k8s-apps-with-kind--benjamin-elder--james-munnelly


🤝 Credits
===
- https://redj.hatenablog.com/entry/2022/04/24/143831
- https://zenn.dev/vampire_yuta/articles/a9806c655a3114