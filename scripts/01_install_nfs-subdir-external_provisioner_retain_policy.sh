#!/bin/bash
set -e

# Load helper functions
source "$(dirname "$0")/00_helpers.sh"


NFS_POD_IP=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' nfs-pod-nfs-server-1)

warning "This helm installation will retain the PV after the PVC has been deleted"
helm repo add nfs-subdir-external-provisioner https://kubernetes-sigs.github.io/nfs-subdir-external-provisioner/
kubectl create namespace nfs-subdir-external-provisioner  2>/dev/null
helm install nfs-subdir-external-provisioner nfs-subdir-external-provisioner/nfs-subdir-external-provisioner \
--set nfs.server=$NFS_POD_IP --set nfs.path=/export --set storageClass.reclaimPolicy=Retain -n nfs-subdir-external-provisioner

warning "Creating demo deployment in demo namespace..."
kubectl create namespace demo 2>/dev/null
kubectl apply -f ../resources/app/app_with_pvc_using_external_provisioner_storage_class.yaml -n demo