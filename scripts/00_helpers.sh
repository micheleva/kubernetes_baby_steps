#!/bin/bash
set -e

NC='\033[0m' # ANSI color code to reset color
GREEN='\033[0;32m'
RED='\033[0;31m'
YELLOW='\033[1;33m'

failure () {
  echo -e "${RED}${1}${NC}"
}

warning () {
  echo -e "${YELLOW}${1}${NC}"
}

success () {
  echo -e "${GREEN}${1}${NC}"
}

command_exists() {
    command -v "$1" >/dev/null 2>&1
}


check_script_folder() {
    # Path of the folder where the script is located
    local script_folder="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

    # Path of the current working directory
    local current_folder=$(pwd)

    # Since we use relative paths for resources, just abort if we're not in the 'script' folder
    if [ "$script_folder" != "$current_folder" ]; then
        failure "Error: This script must be run from inside the script folder."
        exit 1
    fi
}


check_requirements() {
  # Confirm the script is being run from inside the ./script folder
  check_script_folder

  # Check for either docker-compose or docker command existence
  if ! (command -v docker-compose &> /dev/null || { command -v docker &> /dev/null && docker compose version &> /dev/null; }); then
      failure "Error: Either 'docker-compose' or 'docker' command not found$"
      exit 1
  fi

  # List of required commands
  required_commands=("kind" "kubectl" "helm")

  # Check if all required commands exist
  for cmd in "${required_commands[@]}"; do
      if ! command_exists "$cmd"; then
          failure "Error: '$cmd' command not found. Your environment is not correctly setup for the demo."
          echo ""
          failure "The following commands are required to be installed:"
          for cmd in "${required_commands[@]}"; do
              failure "$cmd"
          done
          echo ""
          failure "It seems you're missing one or more!"
          exit 1
      fi
  done

  if kind get clusters | grep -q "ha"; then
      failure "Kind cluster with name 'ha' already exist! Aborting setup script!"
      exit 1
  fi

  success "Requirements satified!"
}

# Check if a pod exists with the given labels and is in Running state
pod_running() {
    kubectl get pods -n "$NAMESPACE" -l "$LABEL_SELECTOR" 2>/dev/null | grep -q "1/1.*Running"
}
