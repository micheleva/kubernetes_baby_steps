#!/bin/bash
set -e

# Load helper functions
source "$(dirname "$0")/00_helpers.sh"

# Confirm all the needed commands are present, and there are no 'ha' named kind clusters
check_requirements

# Print message in yellow
warning "Creating kubernetes cluster..."
kind create cluster --name ha --config ../resources/kind/1-master-3-workers.yaml

if kind get clusters | grep -q "ha"; then
    success "Kind cluster has been created"
else
    failure "Kind was not setup correctly. Your environment is not correctly setup for the demo."
    exit 1
fi

echo ""
kubectl cluster-info --context kind-ha

echo "Creating a pod exporting an nfs"
mkdir -p /tmp/nfs-root
# Handle docker-compose or docker-compose
docker-compose --file ../resources/nfs-pod/local-container-nfs-server-docker-compose.yaml up -d

echo "Wait for the nfs exporting pod to be up and runnning..."
until docker inspect -f '{{.State.Running}}' nfs-pod-nfs-server-1 2>/dev/null | grep -q true; do
    sleep 1
done

NFS_POD_IP=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' nfs-pod-nfs-server-1)

# TODO: build the client container to test if the nfs is really exported
mkdir -p test-mount-dir
warning "Run 'sudo mount -v -t nfs4 ${NFS_POD_IP}:/ test-mount-dir/' to test that the pod is really exporting an nfs"
warning "(To then umount said nfs share run 'sudo umount test-mount-dir/' )"

# # Check if the mount is successful, and the environment is ready for the demo
# if df -h | grep -q "$NFS_POD_IP"; then

#     success "NFS mount successful with IP address $NFS_POD_IP. Umounting it..."
#     success "Your environment is ready for the demo!"

#     # Attempt to unmount until successful
#     while ! sudo umount -v test-mount-dir/ ; do
#         warning "Unmounting failed. Retrying in 5 seconds..."
#         sleep 5
#     done

#     success "Unmount successful."


# else
#     failure "Error: Unable to find NFS mount with IP address $NFS_POD_IP"
#     failure "Your environment is not correctly setup for the demo!"
#     # Add any additional error handling code here
#     exit 1
# fi

# rmdir test-mount-dir