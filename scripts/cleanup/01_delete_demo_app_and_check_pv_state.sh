#!/bin/bash
set -e

# Load helper functions
source "$(dirname "$0")/../00_helpers.sh"

warning "Removing demo deployment from the demo namespace..."
kubectl delete -f ../resources/app/app_with_pvc_using_external_provisioner_storage_class.yaml -n demo --ignore-not-found

success "Removed demo deployment from the demo namespace!"

# Wait for the deployment to be deleted
while kubectl get deployment demo-busybox -n demo &> /dev/null; do
    warning "Waiting for deployment to be deleted..."
    sleep 5
done

while [[ $(kubectl get pods -n demo -l app=demo -o jsonpath='{.items[*].metadata.name}') ]]; do
    warning "Waiting for pods to be deleted..."
    sleep 5
done


warning "If you have installed the nfs-subdir-external_provisioner with Retain policy, the PV will still be there"

# Check if the PV is still retained
pv_name=$(kubectl get pv -n demo -o json | jq -r '.items[] | select(.spec.claimRef.name == "test-claim") | .metadata.name')
if [ -n "$pv_name" ]; then
    success "PV $pv_name is still retained - as expected. Check out the following output!"
    echo ""
    kubectl get pv $pv_name -n demo
else
    failure "The PV was released. You have likely accidentally run the wrong setup script, and the PV was created with 'reclaimPolicy: Delete'"
    exit 1
fi

