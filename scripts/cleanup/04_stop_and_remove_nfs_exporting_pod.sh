#!/bin/bash
set -e

# Load helper functions
source "$(dirname "$0")/../00_helpers.sh"


docker-compose --file ../resources/nfs-pod/local-container-nfs-server-docker-compose.yaml down

echo ""
warning "IMPORTANT: The nfs contents are not deleted. If you want to delete them, clean the /tmp/nfs-root folder content!"
echo ""
warning "Current content of /tmp/nfs-root folder:"
ls -lah /tmp/nfs-root
rmdir test-mount-dir