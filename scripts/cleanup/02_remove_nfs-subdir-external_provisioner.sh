#!/bin/bash
set -e

# Load helper functions
source "$(dirname "$0")/../00_helpers.sh"

warning "Removing the nfs subdir external provider..."

helm uninstall -n nfs-subdir-external-provisioner nfs-subdir-external-provisioner
kubectl delete namespace nfs-subdir-external-provisioner --ignore-not-found

success "Succesfully removed the nfs subdir external provider."

