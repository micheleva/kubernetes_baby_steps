#!/bin/bash
set -e

# Load helper functions
source "$(dirname "$0")/../00_helpers.sh"

warning "Removing ha cluster from kind"

kind delete cluster --name ha