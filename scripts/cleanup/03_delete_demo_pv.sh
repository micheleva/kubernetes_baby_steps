#!/bin/bash
set -e

# Load helper functions
source "$(dirname "$0")/../00_helpers.sh"

warning "Deleting the retained PV..."

kubeclt delete pv -n demo --all 2&> /dev/null