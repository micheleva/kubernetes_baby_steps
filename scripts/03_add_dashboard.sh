#!/bin/bash

# Load helper functions
source "$(dirname "$0")/00_helpers.sh"

helm repo add kubernetes-dashboard https://kubernetes.github.io/dashboard/
helm install dashboard kubernetes-dashboard/kubernetes-dashboard -n kubernetes-dashboard --create-namespace
kubectl apply -f ../resources/etc/01_dashboard_sa.yaml

# Wait until the pod behind the dashboard-kong-proxy svc is up and running
NAMESPACE="kubernetes-dashboard"
LABEL_SELECTOR="app.kubernetes.io/component=app,app.kubernetes.io/instance=dashboard,app.kubernetes.io/name=kong"

warning "Waiting for the dashboard pod to be up and running.."

# Loop until the pod is found and in Running state
while ! pod_running; do
    sleep 5  # Adjust sleep duration as needed
done

success "The dashboard is now ready!"

success "Run 'kubectl -n kubernetes-dashboard port-forward svc/dashboard-kong-proxy 8443:443'"
success "Then access https://localhost:8443/#/workloads?namespace=default"
warning "Use the following token to access the dashboard!"
# TODO, might need to wait for the dashboard pods to be up?
kubectl -n kubernetes-dashboard create token admin-user