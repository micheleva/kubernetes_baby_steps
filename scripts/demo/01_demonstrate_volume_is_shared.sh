#!/bin/bash
set -e

# Load helper functions
source "$(dirname "$0")/../00_helpers.sh"

# Function to wait until all pods are in the "Running" state
wait_for_running() {
    local pods=$1
    local all_running=false

    while ! $all_running; do
        all_running=true

        for pod in $pods; do
            local pod_status=$(kubectl get $pod -n demo -o jsonpath='{.status.phase}')
            if [ "$pod_status" != "Running" ]; then
                all_running=false
                warning "Waiting for pod $pod to be in the 'Running' state..."
                sleep 5
                break
            fi
        done
    done
}

# Get the list of pods with the label appdb=rsvpdb
pods=$(kubectl get pod -l app=demo -o name -n demo)

# Wait until all pods are in the "Running" state
wait_for_running "$pods"

# Loop through each pod and execute the command inside
for pod in $pods; do
    warning "Pod with name '$pod' is writing to hoge.txt"
    pod_name=$(echo $pod | cut -d'/' -f 2)
    kubectl exec -it $pod -n demo -- /bin/sh -c 'echo "I am '$pod_name'" >> /data/db/hoge.txt'
done

# Display the content of hoge.txt from each pod
for pod in $pods; do
    success "Content of 'hoge.txt' seen from Pod with name '$pod':"
    kubectl exec -it $pod -n demo -- cat /data/db/hoge.txt
    success "--------------------------"
done

